<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CNP-;^E* c|)tFq 1^(Qz&u00uK1.hmrJimU$Bm):eakP)C9,FzH*yv:BaW*S>o4');
define('SECURE_AUTH_KEY',  'SH7PHJbm,W&WVW>x+UU2>dX?SL.Ton~GF*n@qm0mdt{S$R-XJfk}P)EpPbos&8K(');
define('LOGGED_IN_KEY',    'rspXo?+9NZ{dyK%t8L45#$[3tu<kBn2iC5Pbg$-7T% pM$N0Yb-cld-n:M7}o&tE');
define('NONCE_KEY',        'Vv9V9Z[@^HyRGe,9^~P-U|<|i]b-D|<ojWV$+`~m-^1RE?-7-_bO)cH``[1mE^37');
define('AUTH_SALT',        '|sd4fpIN-<:{+z>,HgxSI2vW0@Jp<z@O|)quNa{b@O6?W]PZL}3|w5,{rgZH5t f');
define('SECURE_AUTH_SALT', 'hA~`)^%n(bj-v5WYX-VM+9V|PPy4~&_<I)ADR{){*l=_<>LXf%p3HwNU[IuO@q>B');
define('LOGGED_IN_SALT',   'YLp_99pbvNLRyv_+lk0h(`ih9|=8Rn27|/t37_8v6+V!sq6T9D:-4oP jCC#_jyx');
define('NONCE_SALT',       '.L$9Pb%N~-Q|*s*L A]fB*;E7jH9nv^Emw$aQAS#p]UE_leM-)HbU#5 |0 E3AOk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

